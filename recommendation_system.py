import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

def calculate_cost(m, hypothesis, y):
    """
    This is a linear regression cost function
    :param m number of data points
    :param hypothesis: simple linear hypothesis
    :param y: actual values
    :return cost
    
    """
    cost = (1/(2*m))*np.sum((hypothesis - y)**2)
    return cost

def generate_hypothesis(thetas_0, X):
    """
    Wrapper for hypothesis function
    :param thetas_0: shape must be compatible with data
    :param X: data
    """
    hypothesis = np.dot(thetas_0.T, np.array([X[:, 0], X[:, 1], X[:, 2], X[:, 3]]))
    #hypothesis = np.dot(thetas_0.T, X.T) # Identically the same if no polynomials are needed
    return hypothesis

def gradient_descent(hypothesis, y, X, thetas_0, alpha):
    """
    No regularisation applied
    We do not regularise for thetas_00.
    """
    m = X.shape[0]
    grads = (1/m)*np.sum(np.multiply((hypothesis - y), X.T), axis=1)
    thetas_0 = thetas_0.T - alpha*grads
    return thetas_0.T

def test_linearRegressin(X, y, alpha, iterations):

    m = X.shape[0]
    n = X.shape[1]
    thetas_0 = np.zeros([n, 1])

    cost_array = []
    for i in range(iterations):
        """
        training data: age, height, weight
        target variable: networth
        """
        hypothesis = generate_hypothesis(thetas_0, X)
        cost = calculate_cost(m, hypothesis, y)
        
        thetas_0 = gradient_descent(hypothesis, y, X, thetas_0, alpha)

        cost_array.append(cost)

    plt.plot(range(iterations), cost_array)
    print (thetas_0)


#Collaborative Filtering
movies = np.array(["Notting Hill", "Titanic", "Beauty and the Beast", "Iron Man", "Batman", "Taken", "WW2"])
users = np.array(["James", "Winson", "Lauren", "Gareth", "Daniel"])
genreTypes = ["romance", "action", "history"]

df_movieGenre = pd.DataFrame([
    [5, 0, 2],
    [5, 0, 4],
    [5, 0, 0],
    [0, 5, 0],
    [0, 5, 0],
    [0, 5, 0],
    [0, 0, 5]], columns = genreTypes, index= movies)


df_userRatings = pd.DataFrame([
    [0, 0, 0, 5, 5, np.nan, np.nan],
   [0, 0, 0, np.nan, 5, 5, np.nan],
   [5, 5, np.nan, 0, 0, 0, np.nan],
    [5, 5, np.nan, 0, 0, 0, np.nan],
    [0, 0, 0, 0, 0, 0, 5]], columns = movies, index = users)


users = np.array(["James", "Winson", "Lauren", "Gareth", "Daniel"])
ratings = df_userRatings.values
genre = df_movieGenre.values

### ------------ (1) Predict user based on movie genre -------------- ###
r = np.isnan(ratings)
n_movies = len(movies)
features_genre = np.concatenate([np.ones([n_movies, 1]), genre], axis = 1)

m = features_genre.shape[0]

alpha = 0.00001
thetas_0 = np.zeros([ratings.shape[0], features_genre.shape[1]]) # shape (users x 5, genres x 3 + intercept)
iterations = range(10000)
cost_list = []

for i in iterations:

    hypothesis = np.dot(thetas_0, features_genre.T)
    ratings[r] = 0 # all missing data is assigned with 0    
    hypothesis[r] = 0 # all hypothesis of missing data is assigned with 0

    cost = (1/(2*m))*np.sum((hypothesis - ratings)**2)
    cost_list.append(cost)

    thetas_0 = thetas_0 - (alpha * np.dot((hypothesis - ratings), features_genre))


print ("users: {}".format(users))

prediction =  (np.dot(thetas_0, features_genre.T).round(2))
preference = prediction.dot(genre)
indices = np.argmax(preference, axis = 1)
map_genres = np.array(genreTypes)[indices]
print (dict(zip(users, map_genres)))


### ------------ (2) Predict genre based on user ratings -------------- ###

features_ratings = np.concatenate([np.ones([ratings.shape[0], 1]), ratings], axis = 1)
thetas_1 = np.zeros([len(genreTypes), features_ratings.shape[1]]) # shape (3 movie genres, ratings x 7 + intercept)

for i in iterations:

    hypothesis = np.dot(thetas_1, features_ratings.T)

    #cost = (1/(2*m))*np.sum((hypothesis - ratings)**2)
    #cost_list.append(cost)

    thetas_1 = thetas_1 - (alpha * np.dot((hypothesis - preference.T), features_ratings))



while True:
    inp = input("1. Recommend Movie \n2. Rate a movie\n->")
    if int(inp) == 1:
        print ("Please tell us about this movie, what is the genre between 0-5?")
        genre_ = np.ones(len(genreTypes) + 1)
        n = 1
        for gen in genreTypes:
            inp = input("{}: ".format(gen))
            if inp.lower() == 'quit':
                break
            genre_[n] = inp
            n +=1
        pred = (np.dot(thetas_0, genre_.T))
        print (pred)
        max_i = np.argmax(pred)
        print ("I think {} will like this movie".format(users[max_i]))
        print ()
    elif int(inp) == 2:
        print("Please rating the following movies:")
        user_ratings = np.ones(len(movies) + 1)
        n = 1
        for movie in movies:
            inp = input("{}: ".format(movie))
            if inp.lower() == 'quit':
                break
            user_ratings[n] = inp
            n += 1
        predict_genre = np.dot(thetas_1, np.array(user_ratings))
        argmax = np.argmax(predict_genre)
        print("I think you will like {}".format(genreTypes[argmax]))
        print ()
    else:
        print ("invalid option")
        continue
